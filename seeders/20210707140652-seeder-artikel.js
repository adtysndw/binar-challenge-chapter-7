"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "artikels",
      [
        {
          judul: "Cara Install Adobe",
          kontent:
            "Cara install adobe lengkap diantara adobe photoshop, lightroom, Illustrator dan lainnya",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          judul: "Cara Menggunakan Microsoft Office",
          kontent:
            "Panduan cara menggunakan microsoft office dari dasar hingga mahir lengkap bergaransi",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          judul: "Belajar Bahasa Javascript",
          kontent:
            "Belahar bahasa javascript dari dasar lengkap",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("artikels", null, {});
  },
};
