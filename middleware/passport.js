const passport = require("passport");
const bcrypt = require("bcrypt");
var LocalStrategy = require("passport-local");
const { user } = require("../models");

passport.use(
  new LocalStrategy(function (username, password, cb) {
    user
      .findOne({
        where: { name: username },
        raw: true,
      })
      .then((data) => {
        if (!data) {
          return cb(null, false);
        }
        console.log("user", data);

        // Function defined at bottom of app.js
        const isValid = bcrypt.compareSync(password, data.password);
        console.log("isValid", isValid);

        if (isValid) {
          return cb(null, data);
        } else {
          return cb(null, false);
        }
      })
      .catch((err) => {
        cb(err);
      });
  })
);

passport.serializeUser(function (data, cb) {
  cb(null, data.id);
});

passport.deserializeUser(function (id, cb) {
  // cb(null, user);
  user
    .findByPk(id, {
      raw: true,
    })
    .then((data) => cb(null, data));
});
// };
